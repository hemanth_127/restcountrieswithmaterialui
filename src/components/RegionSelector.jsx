import Box from '@mui/material/Box'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import Select from '@mui/material/Select'
import React, { useContext } from 'react'
import { GlobalContext } from '../GlobalContext'

const RegionSelector = () => {
  const { countries, selectedRegion, setSelectedRegion, setSelectedSubRegion } =
    useContext(GlobalContext)

  const getRegions = () => {
    let regions = []

    countries.forEach(country => {
      if (country.region && !regions.includes(country.region)) {
        regions.push(country.region)
      }
    })
    return regions.sort()
  }

  const handleRegionChange = event => {
    setSelectedRegion(event.target.value)
    setSelectedSubRegion('')
  }
  return (
    <Box sx={{ m: 1 }}>
      <FormControl sx={{ width: '11rem' }}>
        <InputLabel id='demo-simple-select-label'>Region</InputLabel>
        <Select
          labelId='demo-simple-select-label'
          id='demo-simple-select'
          label='Region'
          onChange={handleRegionChange}
          value={selectedRegion}
        >
          <MenuItem key={0} value=''>
            All{' '}
          </MenuItem>
          {getRegions().map((region, index) => (
            <MenuItem key={index} value={region}>
              {region}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  )
}

export default RegionSelector

// const RegionSelector = () => {
//   const { countries, selectedRegion, setSelectedRegion, setSelectedSubRegion } =
//     useContext(GlobalContext)

//   const getRegions = () => {
//     let regions = []

//     countries.forEach(country => {
//       if (country.region && !regions.includes(country.region)) {
//         regions.push(country.region)
//       }
//     })
//     return regions.sort()
//   }

//   const handleRegionChange = event => {
//     setSelectedRegion(event.target.value)
//     setSelectedSubRegion('')
//   }

//   return (
//     <select onChange={handleRegionChange} value={selectedRegion}>
//       <option value='select' disabled>
//         Select Region
//       </option>
//       <option value=''>ALL Regions </option>
//       {getRegions().map((region, index) => (
//         <option key={index} value={region}>
//           {region}
//         </option>
//       ))}
//     </select>
//   )
// }

// export default RegionSelector
