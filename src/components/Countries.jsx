import { styled } from '@mui/material/styles'
import Grid from '@mui/material/Grid'
import Paper from '@mui/material/Paper'
import Box from '@mui/material/Box'

import React, { useContext } from 'react'
import { GlobalContext } from '../GlobalContext'
import { Link, useNavigate } from 'react-router-dom'
import CircularProgress from '@mui/material/CircularProgress'

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  color: theme.palette.text.secondary
}))

export default function Countries () {
  const navigate = useNavigate()
  const { filterData } = useContext(GlobalContext)

  function handleDetailView (ccn3) {
    navigate(`country/${ccn3}`)
  }

  if (filterData.length <= 0) {
    return (
      <CircularProgress disableShrink sx={{ marginLeft: 80, marginTop: 20 }} />
    )
  }

  return (
    <>
      <Box sx={{ width: '100%' }}>
        <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
          {filterData.map(country => {
            const {
              name: { common: name },
              population,
              region,
              capital,
              ccn3,
              flags: { png: flags }
            } = country
            return (
              <Grid item xs={3}>
                <Item onClick={() => handleDetailView(ccn3)} key={ccn3}>
                  <Link to={`country/${ccn3}`}>
                    <img
                      className='image'
                      src={flags}
                      alt={name}
                      style={{ width: '100%', height: 200 }}
                    />
                  </Link>
                  <div className='article-div'>
                    <h3>{name}</h3>
                    <p>
                      Population: <span>{population}</span>
                    </p>
                    <p>
                      Region: <span>{region}</span>
                    </p>
                    <p>
                      Capital: <span>{capital}</span>
                    </p>
                  </div>
                </Item>
              </Grid>
            )
          })}
        </Grid>
      </Box>
    </>
  )
}

// import React, { useContext, useEffect } from 'react';
// import { GlobalContext } from '../GlobalContext';
// import { useNavigate } from 'react-router-dom';

// const Countries = () => {
//   const navigate = useNavigate();
//   const { filterData, setDetailview,searchTerm } = useContext(GlobalContext);

//   function handleDetailView (country) {
//     setDetailview(country.ccn3)
//     navigate(`country/${country.ccn3}`)
//   }

//   if (filterData.length<0) {
//     return <p>Loading...</p>;
//   }

//   return (
//     <>
//       {filterData.length === 0 ? (
//         <p style={{
//           color: 'red',
//           marginTop: '6rem',
//           fontSize: '2rem',
//           width: '80vw',
//           display: 'flex',
//           justifyContent: 'center'
//         }}>
//            Not Found {searchTerm}
//         </p>
//       ) : (
//         filterData.map(country => {
//           const {
//             name: { common: name },
//             population,
//             region,
//             capital,
//             cca3,
//             flags: { png: flags }
//           } = country;
//           return (
//             <article
//               key={cca3}
//               className='article'
//               onClick={() => handleDetailView(country)}
//             >
//               <img
//                 className='image'
//                 src={flags}
//                 alt={name}
//                 style={{ width: '100%', height: 200 }}
//               />
//               <div className='article-div'>
//                 <h3>{name}</h3>
//                 <p>
//                   Population: <span>{population}</span>
//                 </p>
//                 <p>
//                   Region: <span>{region}</span>
//                 </p>
//                 <p>
//                   Capital: <span>{capital}</span>
//                 </p>
//               </div>
//             </article>
//           );
//         })
//       )}
//     </>
//   );
// };

// export default Countries;
