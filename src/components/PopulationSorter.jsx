import React from 'react'
import { useContext } from 'react'
import Box from '@mui/material/Box'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import Select from '@mui/material/Select'
import { GlobalContext } from '../GlobalContext'

const PopulationSorter = () => {
  const { setSortByPopulation, setSortByArea } = useContext(GlobalContext)
  const handlePopulationSort = event => {
    setSortByArea('')
    setSortByPopulation(event.target.value)
  }

  return (
    <Box sx={{ m:1 }}>
      <FormControl sx={{ width: '11rem' }}>
        <InputLabel id='demo-simple-select-label'>SortBy Population</InputLabel>
        <Select
          labelId='demo-simple-select-label'
          id='demo-simple-select'
          label='SortBy Population'
          onChange={handlePopulationSort}
        >
          <MenuItem key={2} value='Asc'>
            Ascending
          </MenuItem>
          <MenuItem key={3} value='Desc'>
            Descending
          </MenuItem>
        </Select>
      </FormControl>
    </Box>
  )
}

export default PopulationSorter

// import React, { useContext } from 'react';
// import { GlobalContext } from '../GlobalContext'; // Adjust the path accordingly

// const PopulationSorter = () => {
//   const { setSortByPopulation,setSortByArea } = useContext(GlobalContext);

//   const handlePopulationSort = event => {
//     setSortByArea('')
//     setSortByPopulation(event.target.value);
//   };

//   return (
//     <div>
//       <select name='population' id='hel' onChange={handlePopulationSort}>
//         <option value=''>None</option>
//         <option value='Asc'>Ascending</option>
//         <option value='Desc'>Descending</option>
//       </select>
//     </div>
//   );
// };

// export default PopulationSorter;
