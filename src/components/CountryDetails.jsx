import Button from '@mui/material/Button'
import Stack from '@mui/material/Stack'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import Heading from './Heading'
import React from 'react'
import { GlobalContext } from '../GlobalContext'
import { useContext, useState, useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import CircularProgress from '@mui/material/CircularProgress'

export default function CountryDetails () {
  const navigate = useNavigate()
  const [details, setDetails] = useState([])
  const { id } = useParams()
  const { countries } = useContext(GlobalContext)

  useEffect(() => {
    const fetchCountry = async () => {
      try {
        const response = await fetch(
          `https://restcountries.com/v3.1/alpha/${id}`
        )
        if (!response.ok) {
          throw new Error("Couldn't fetch country data")
        }
        const data = await response.json()
        setDetails(data)
      } catch (error) {
        setError(error.message)
      }
    }
    fetchCountry()
  }, [])
  if (details.length <= 0) {
    return <CircularProgress disableShrink sx={{ ml: 90, mt: 20 }} />
  }
  const {
    name,
    population,
    region,
    capital,
    subregion,
    currencies,
    languages,
    flags: { png: flags },
    borders
  } = details[0]

  return (
    <>
      <div>
        <Heading />
        <Stack direction='row' sx={{ m: 9 }}>
          <Button
            variant='contained'
            onClick={() => {
              navigate(-1)
            }}
          >
            <ArrowBackIcon />
            &nbsp; BACK
          </Button>
        </Stack>
        <div className='detai' style={{ margin: 70 }}>
          <div className='img'>
            <img
              src={flags}
              alt='flag'
              style={{
                width: '35vw',
                height: 350
              }}
            />
          </div>

          <div className='info' style={{ width: '50%', paddingTop: '2rem' }}>
            <div className='info-header'>
              <h3>{name.common}</h3>
            </div>
            <div
              className='info-information'
              style={{ display: 'flex', justifyContent: 'space-between' }}
            >
              <div className='Country'>
                <p>
                  nativeName: &nbsp;
                  {name?.nativeName?.tur?.common ||
                    name?.nativeName?.eng?.common ||
                    'not available'}
                </p>
                <p>
                  Population: <span>{population}</span>
                </p>
                <p>
                  Region: <span>{region}</span>
                </p>
                <p>subRegion: {subregion}</p>
                <p>
                  Capital: <span>{capital}</span>
                </p>
              </div>
              <div className='extra'>
                <p>
                  Currencies: &nbsp;
                  {currencies
                    ? Object.keys(currencies).map(currency => (
                        <span key={currency}>
                          {currencies[currency].name} ({currency})
                        </span>
                      ))
                    : 'Not Availavle'}
                </p>
                <p>
                  Languages: &nbsp;{' '}
                  {languages
                    ? Object.values(languages).join(', ')
                    : 'Not Available'}
                </p>
              </div>
            </div>
            <div
              className='info-borders'
              style={{ paddingTop: '1rem', marginRight: '1rem' }}
            >
              Borders Countries :&nbsp;
              {borders
                ? borders.map((border, index) => (
                    <Button
                      variant='inherit'
                      key={index}
                      onClick={() => {}}
                      sx={{ m: 1, backgroundColor: ' hsl(0, 0%, 98%);' }}
                    >
                      {border}
                    </Button>
                  ))
                : 'not available'}
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

// import React from 'react'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
// import { GlobalContext } from '../GlobalContext'
// import { useContext } from 'react'
// import Heading from './Heading'
// import { useNavigate } from 'react-router-dom'

// const CountryDetails = () => {
//   const navigate = useNavigate()
//   const { detailview, countries } = useContext(GlobalContext)

//   const details = countries.filter(country => {
//     return country.ccn3 === detailview
//   })

//   const {
//     name,
//     population,
//     region,
//     capital,
//     subregion,
//     currencies,
//     languages,
//     flags: { png: flags },
//     borders
//   } = details[0]

//   return (
//     <>
//       <div className='row'>
//         <Heading />
//         <div
//           className='detail-btn'
//           onClick={() => {
//             navigate(-1)
//           }}
//         >
//           <FontAwesomeIcon icon={faArrowLeft} />
//           <button className='navbar-btn'>Back</button>
//         </div>

//         <div className='detail'>
//           <div>
//             <img
//               className='image'
//               src={flags}
//               alt='flag'
//               style={{
//                 width: '30vw',
//                 height: 300
//               }}
//             />
//           </div>

//           <article className='detail-articles'>
//             <div className='detail-article'>
//               <div className='detail-article-div'>
//                 <h3>{name.common}</h3>

//                 <p>
//                   nativeName: &nbsp;
//                   {name?.nativeName?.tur?.common ||
//                     name?.nativeName?.eng?.common ||
//                     'not available'}
//                 </p>

//                 <p>
//                   {' '}
//                   Population: <span>{population}</span>{' '}
//                 </p>
//                 <p>
//                   {' '}
//                   Region: <span>{region}</span>{' '}
//                 </p>
//                 <p>subRegion: {subregion}</p>
//                 <p>
//                   {' '}
//                   Capital: <span>{capital}</span>{' '}
//                 </p>
//               </div>
//               <div className='detail-article-div art'>
//                 <p>
//                   Currencies: &nbsp;
//                   {currencies
//                     ? Object.keys(currencies).map(currency => (
//                         <span key={currency}>
//                           {currencies[currency].name} ({currency})
//                         </span>
//                       ))
//                     : ''}
//                 </p>
//                 <p>
//                   {' '}
//                   Languages: &nbsp;{' '}
//                   {languages ? Object.values(languages).join(', ') : ''}
//                 </p>
//               </div>
//             </div>
//             <div className='detail-article2'>
//               <p>
//                 borders :&nbsp;
//                 {borders
//                   ? borders.map((border, index) => (
//                       <button key={index}>{border}</button>
//                     ))
//                   : 'not available'}
//               </p>
//             </div>
//           </article>
//         </div>
//       </div>
//     </>
//   )
// }

// export default CountryDetails
