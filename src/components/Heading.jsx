import React,{useState} from 'react'
import AppBar from '@mui/material/AppBar'
import Box from '@mui/material/Box'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import DarkModeOutlinedIcon from '@mui/icons-material/DarkModeOutlined';



const Heading = () => {
  const [darkMode, setDarkMode] = useState(false)
    const changeTheme = () => {
      document.body.classList.toggle('dark')
      setDarkMode(!darkMode)
    }
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position='static'  color="transparent" sx={{width:'100vw'}}>
        <Toolbar>
          <Typography variant='h6' component='div' sx={{ flexGrow: 1 }}>
            Where in the world
          </Typography>
          <DarkModeOutlinedIcon  onClick={changeTheme}/>
          <Button variant='inherit' sx={{ backgroundColor: 'none' }}  onClick={changeTheme}> Dark Mode</Button>
        </Toolbar>
      </AppBar>
    </Box>
  )
}

export default Heading
