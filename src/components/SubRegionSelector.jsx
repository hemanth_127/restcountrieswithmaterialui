import Box from '@mui/material/Box'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import Select from '@mui/material/Select'
import React, { useContext } from 'react'
import { GlobalContext } from '../GlobalContext'

export default function SubRegionSelector () {
  const { countries, selectedRegion, selectedSubRegion, setSelectedSubRegion } =
    useContext(GlobalContext)

  const subregions = () => {
    let subRegion = []
    countries.forEach(country => {
      if (
        country.region === selectedRegion &&
        country.subregion &&
        !subRegion.includes(country.subregion)
      ) {
        subRegion.push(country.subregion)
      }
    })
    return subRegion.sort()
  }

  const handleSubRegionChange = event => {
    setSelectedSubRegion(event.target.value)
  }

  return (
    <Box sx={{ m: 1 }}>
      <FormControl sx={{ width: '11rem' }}>
        <InputLabel id='demo-simple-select-label'>subregion</InputLabel>
        <Select
          labelId='demo-simple-select-label'
          id='demo-simple-select'
          label='subregion'
          onChange={handleSubRegionChange}
          value={selectedSubRegion}
        >
          <MenuItem key={0} value=''>
            All{' '}
          </MenuItem>
          {subregions().map((subregion, index) => (
            <MenuItem key={index} value={subregion}>
              {subregion}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  )
}

// import React, { useContext } from 'react';
// import { GlobalContext } from '../GlobalContext'; // Adjust the path accordingly

// const SubRegionSelector = () => {
//   const { countries, selectedRegion, selectedSubRegion, setSelectedSubRegion } = useContext(GlobalContext);

//   // Function to get subregions based on selected region
//   const subregions = () => {
//     let subRegion = [];
//     countries.forEach(country => {
//       if (country.region === selectedRegion && country.subregion && !subRegion.includes(country.subregion)) {
//         subRegion.push(country.subregion);
//       }
//     });
//     return subRegion.sort();
//   };

//   // Handler for subregion change
//   const handleSubRegionChange = event => {
//     setSelectedSubRegion(event.target.value);
//   };

//   return (
//     <select onChange={handleSubRegionChange} value={selectedSubRegion}>
//       <option value='' disabled>
//         Select Subregion
//       </option>
//       <option value=''>All SubRegions</option>
//       {subregions().map((subregion, index) => (
//         <option key={index} value={subregion}>{subregion}</option>
//       ))}
//     </select>
//   );
// };

// export default SubRegionSelector;

// import React from 'react'

// const SubRegionSelector = ({countries,selectedRegion,selectedSubRegion,setSelectedSubRegion}) => {

//   //subregions
//   const subregions = () => {
//     let subRegion = []
//     countries.forEach(country => {
//       if (country.region === selectedRegion && country.subregion && !subRegion.includes(country.subregion)) {
//            subRegion.push(country.subregion)
//         }
//     })
//     return subRegion.sort()
//   }

//   const handleSubRegionChange = event => {
//     setSelectedSubRegion(event.target.value )
//   }

//   return (
//     <select onChange={handleSubRegionChange} value={selectedSubRegion}>
//       <option value='' disabled>
//         Select Subregion
//       </option>
//       <option value=''>All SubRegions</option>
//       {subregions().map((subregion, index) => (
//         <option key={index} value={subregion}>{subregion}</option>
//       ))}
//     </select>
//   )
// }

// export default SubRegionSelector
