import React, { useContext } from 'react'
import { GlobalContext } from '../GlobalContext'
import Box from '@mui/material/Box'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import Select from '@mui/material/Select'

const AreaSort = () => {
  const { setSortByArea, setSortByPopulation } = useContext(GlobalContext)

  const handleAreaSort = event => {
    setSortByPopulation('')
    setSortByArea(event.target.value)
  }

  return (
    <Box sx={{ m:1 }}>
      <FormControl sx={{ width: '11rem' }}>
        <InputLabel id='demo-simple-select-label'> SortByArea</InputLabel>
        <Select
          labelId='demo-simple-select-label'
          id='demo-simple-select'
          label=' SortByArea'
          onChange={handleAreaSort}
        >
          <MenuItem key={2} value='Asc1'>
            Ascending
          </MenuItem>
          <MenuItem key={3} value='Desc1'>
            Descending
          </MenuItem>
        </Select>
      </FormControl>
    </Box>
  )
}

export default AreaSort

// import React, { useContext } from 'react';
// import { GlobalContext } from '../GlobalContext'; // Adjust the path accordingly

// const AreaSort = () => {
//   const { setSortByArea,setSortByPopulation} = useContext(GlobalContext);

//   // Handler for area sorting
//   const handleAreaSort = event => {
//     setSortByPopulation('')
//     setSortByArea(event.target.value);
//   };

//   return (
//     <div>
//       Sort Area
//       <select name='area' id='hel' onChange={handleAreaSort}>
//         <option value=''>None</option>
//         <option value='Asc1'>Ascending</option>
//         <option value='Desc1'>Descending</option>
//       </select>
//     </div>
//   );
// };

// export default AreaSort;
